using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace homework111
{
    class Program
    {
        static void Main(string[] args)
        {
            int a = 28;
            Console.WriteLine(newMath.IsPrime(5));
            int[] arr = { 12, 13, 14, 15 };
            Console.WriteLine(arr.Max());
            Console.WriteLine(arr.Min());
            Console.WriteLine(newMath.Sum(arr));
            Console.WriteLine(newMath.IsPerfect(6));
            Console.WriteLine(arr.Average());
            Console.WriteLine(newMath.SumOfDigits(936));
            Print(arr);
            string[] arr3 = { "aaaa", "bbbb", "cccc", "dddd", "eeee", "ffff", "gggg", "hhhh", "iiii", "jjjj" };
            newMath.Swap(arr3, 1, 3);
            Print(arr3);
            newMath.Shuffle(arr);
            Print(arr);
            Console.WriteLine();
            Console.WriteLine();

        }
        static void Print<T>(T[] arr)
        {
            for (int i = 0; i < arr.Length; i++)
            {
                Console.WriteLine($"[{i}]={arr[i]}");
            }
            Console.WriteLine();
        }
    }
   static class newMath
    {
       
    
            public static bool IsPrime(int value)
            {
                if (value == 2)
                    return true;
                if (value < 2 || value % 2 == 0)
                    return false;
                else
                    for (int i = 3; i < Math.Sqrt(value); i += 2)
                    {
                        if (value % i == 0)
                            return false;
                    }
                return true;
            }

            public static bool IsPerfect(int value)
            {
                int sum = 0;
                for (int i = 1; i <= value / 2; i++)
                {
                    if (value % i == 0)
                        sum += i;
                }
                if (sum == value)
                    return true;
                else
                    return false;
            }

            public static int SumOfDigits(int value)
            {
                int sum = 0;
                int num = 0;
                while (value != 0)
                {
                    num = value % 10;
                    sum += num;
                    value /= 10;
                }
                return sum;
            }

            public static int Sum(int[] arr)
            {
                int sum = 0;
                for (int i = 0; i < arr.Length; i++)
                {
                    sum += arr[i];
                }
                return sum;
            }

            public static double Average(int[] arr)
            {
                int length = arr.Length;
                int sum = 0;
                for (int i = 0; i < length; i++)
                {
                    sum += arr[i];
                }
                double aver = sum / length;
                return aver;
            }

            public static int Max(this int[] arr)
            {
                int max = arr[0];
                for (int i = 1; i < arr.Length; i++)
                {
                    if (max < arr[i])
                        max = arr[i];
                }
                return max;
            }

            public static int Min(this int[] arr)
            {
                int min = arr[0];
                for (int i = 1; i < arr.Length; i++)
                {
                    if (min > arr[i])
                        min = arr[i];
                }
                return min;
            }

            public static void Swap<T>(T[] arr, int index1, int index2)
            {
                T num = arr[index1];
                arr[index1] = arr[index2];
                arr[index2] = num;
            }

            public static void Shuffle<T>(T[] arr)
            {
                Random rand = new Random();
                for (int i = 0; i < arr.Length; i++)
                {
                    T temp = arr[i];
                    int r = rand.Next(i, arr.Length);
                    arr[i] = arr[r];
                    arr[r] = temp;
                }
            }
        }
    
